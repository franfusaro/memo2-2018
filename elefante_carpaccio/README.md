Elefante Carpaccio
==================

El ejercicio está implementado similarmente al de Sinatra.

Para su uso se debe levantar la app mediante:
bundle exec ruby app.rb

Y luego, mediante CURL, hacerle las correspondientes consultas.
Ejemplo: curl 'localhost:4567/calc_discount?state=NV&price=520&quantity=10'
