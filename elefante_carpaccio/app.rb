require 'sinatra'
require 'json'
require_relative 'model/calculator.rb'

get '/calc_discount' do
  content_type :json
  price = params["price"]
  state = params["state"]
  quantity = (params["quantity"].nil? ? 1 : params["quantity"])
  if price.nil? || state.nil? || quantity.to_i < 0 || price.to_i < 0
    { :error => "Error: Invalid parameters"}.to_json
  else
    calculator = PriceCalculator.new
    total_price = calculator.calculate_total_price(price, quantity, state)
    { :total_price => total_price }.to_json
  end
end
