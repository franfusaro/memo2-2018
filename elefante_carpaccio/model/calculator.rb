class PriceCalculator
  
  def initialize()
    @states = { "UT" => 0.0685, "NV" => 0.08, "TX" => 0.0625, "AL" => 0.04, "CA" => 0.0825 }
    @discounts_starting_prices = [ 0, 1000, 5000, 7000, 10000, 50000 ]
    @discounts = [ 0, 0.03, 0.05, 0.07, 0.1, 0.15 ]
  end
  
  def calculate_total_price(price, quantity, state)
    tax_rate = 1.0
    price_discount = 1.0
    if @states.include? state
      tax_rate += @states[state]
    end
    price_without_discount = price.to_f * quantity.to_i * tax_rate
    for i in (@discounts.length).downto(1)
      if @discounts_starting_prices[i-1].to_f <= price_without_discount
        price_discount -= @discounts[i-1]
        break
      end
    end
    total_price = price_without_discount * price_discount
    return total_price.round(3)
  end
  
end
