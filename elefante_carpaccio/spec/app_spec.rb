# spec/app_spec.rb

require File.expand_path '../spec_helper.rb', __FILE__

describe "Sinatra Application" do

  it "get /calc_discount with empty values should return an invalid parameters error" do
    get '/calc_discount'
    expect(last_response).to be_ok
    expect(last_response.body).to include 'error'
    result = JSON.parse(last_response.body)
    expect(result["error"]).to eq 'Error: Invalid parameters'
  end
  
  it "get /calc_discount with price but no state return an invalid parameters error" do
    get '/calc_discount?price=500'
    expect(last_response).to be_ok
    expect(last_response.body).to include 'error'
    result = JSON.parse(last_response.body)
    expect(result["error"]).to eq 'Error: Invalid parameters'
  end

  it "get /calc_discount with state but no price return an invalid parameters error" do
    get '/calc_discount?state=UT'
    expect(last_response).to be_ok
    expect(last_response.body).to include 'error'
    result = JSON.parse(last_response.body)
    expect(result["error"]).to eq 'Error: Invalid parameters'
  end
  
  it "get /calc_discount with negative quantity should return an invalid parameters error" do
    get '/calc_discount?price=-37&quantity=12&state=NV'
    expect(last_response).to be_ok
    expect(last_response.body).to include 'error'
    result = JSON.parse(last_response.body)
    expect(result["error"]).to eq 'Error: Invalid parameters'
  end
  
  it "get /calc_discount with negative price should return an invalid parameters error" do
    get '/calc_discount?price=90&quantity=-1&state=NV'
    expect(last_response).to be_ok
    expect(last_response.body).to include 'error'
    result = JSON.parse(last_response.body)
    expect(result["error"]).to eq 'Error: Invalid parameters'
  end

  it "get /calc_discount with price 100 and state UT should return a total price of 106.85" do
    get '/calc_discount?price=100&state=UT'
    expect(last_response).to be_ok
    expect(last_response.body).to include 'total_price'
    result = JSON.parse(last_response.body)
    expect(result['total_price']).to eq 106.85
  end

  it "get /calc_discount with price 100 and state NV should return a total price of 108" do
    get '/calc_discount?price=100&state=NV'
    expect(last_response).to be_ok
    expect(last_response.body).to include 'total_price'
    result = JSON.parse(last_response.body)
    expect(result['total_price']).to eq 108
  end
  
  it "get /calc_discount with price 250 and state TX should return a total price of 265.625" do
    get '/calc_discount?price=250&state=TX'
    expect(last_response).to be_ok
    expect(last_response.body).to include 'total_price'
    result = JSON.parse(last_response.body)
    expect(result['total_price']).to eq 265.625
  end
  
  it "get /calc_discount with price 100 and state CA should return a total price of 108.25" do
    get '/calc_discount?price=100&state=CA'
    expect(last_response).to be_ok
    expect(last_response.body).to include 'total_price'
    result = JSON.parse(last_response.body)
    expect(result['total_price']).to eq 108.25
  end
  
  it "get /calc_discount with quantity 10, price 90 and state NV should return a total price of 972" do
    get '/calc_discount?price=90&quantity=10&state=NV'
    expect(last_response).to be_ok
    expect(last_response.body).to include 'total_price'
    result = JSON.parse(last_response.body)
    expect(result['total_price']).to eq 972
  end
  
  it "get /calc_discount with quantity 100, price 12 and state UT should return a total price of 1243.734" do
    get '/calc_discount?price=12&quantity=100&state=UT'
    expect(last_response).to be_ok
    expect(last_response.body).to include 'total_price'
    result = JSON.parse(last_response.body)
    expect(result['total_price']).to eq 1243.734
  end
  
  it "get /calc_discount with quantity 1, price 5200 and state NV should return a total price of 5335.2" do
    get '/calc_discount?price=5200&quantity=1&state=NV'
    expect(last_response).to be_ok
    expect(last_response.body).to include 'total_price'
    result = JSON.parse(last_response.body)
    expect(result['total_price']).to eq 5335.2
  end
  
  it "get /calc_discount with quantity 3, price 2500 and state CA should return a total price of 7550.437" do
    get '/calc_discount?price=2500&quantity=3&state=CA'
    expect(last_response).to be_ok
    expect(last_response.body).to include 'total_price'
    result = JSON.parse(last_response.body)
    expect(result['total_price']).to eq 7550.437
  end
  
  it "get /calc_discount with quantity 200, price 65.5 and state AL should return a total price of 12261.6" do
    get '/calc_discount?price=65.5&quantity=200&state=AL'
    expect(last_response).to be_ok
    expect(last_response.body).to include 'total_price'
    result = JSON.parse(last_response.body)
    expect(result['total_price']).to eq 12261.6
  end
  
  it "get /calc_discount with quantity 5000, price 10.5 and state TX should return a total price of 47414.063" do
    get '/calc_discount?price=10.5&quantity=5000&state=TX'
    expect(last_response).to be_ok
    expect(last_response.body).to include 'total_price'
    result = JSON.parse(last_response.body)
    expect(result['total_price']).to eq 47414.063
  end
  
end

