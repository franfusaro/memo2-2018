require 'rspec' 
require_relative '../model/calculator.rb'

describe 'PriceCalculator' do
  let(:calculator) { PriceCalculator.new }
  
  it 'calculate_total_price with price 100 and state UT should be 106.85' do
    expect(calculator.calculate_total_price(100,1,"UT")).to eq 106.85
  end
  
  it 'calculate_total_price with price 100 and state NV should be 108' do
    expect(calculator.calculate_total_price(100,1,"NV")).to eq 108
  end
  
  it 'calculate_total_price with price 250 and state TX should be 108' do
    expect(calculator.calculate_total_price(250,1,"TX")).to eq 265.625
  end
  
  it 'calculate_total_price with price 562 and state AL should be 584.48' do
    expect(calculator.calculate_total_price(562,1,"AL")).to eq 584.48
  end
  
  it 'calculate_total_price with price 100 and state CA should be 108.25' do
    expect(calculator.calculate_total_price(100,1,"CA")).to eq 108.25
  end
  
  it 'calculate_total_price with price 90, quantity 10 and state NV should be 972' do
    expect(calculator.calculate_total_price(90,10,"NV")).to eq 972
  end
  
  it 'calculate_total_price with price 100, quantity 12 and state UT should be 1243.734' do
    expect(calculator.calculate_total_price(100,12,"UT")).to eq 1243.734
  end
  
  it 'calculate_total_price with price 5200, quantity 1 and state NV should be 5335.2' do
    expect(calculator.calculate_total_price(5200,1,"NV")).to eq 5335.2
  end
  
  it 'calculate_total_price with price 2500, quantity 3 and state CA should be 7550.437' do
    expect(calculator.calculate_total_price(2500,3,"CA")).to eq 7550.437
  end
  
  it 'calculate_total_price with price 65.5, quantity 200 and state AL should be 12261.6' do
    expect(calculator.calculate_total_price(65.5,200,"AL")).to eq 12261.6
  end
  
  it 'calculate_total_price with price 10.5, quantity 5000 and state TX should be 47414.063' do
    expect(calculator.calculate_total_price(10.5,5000,"TX")).to eq 47414.063
  end
  
end
