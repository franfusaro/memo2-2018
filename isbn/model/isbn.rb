module Errors
  ERR_VACIO = -1
  ERR_CORTO = -2
  ERR_LARGO = -3
  ERR_NO_ES_NUMERICO = -4
end


class Isbn

  ISBN_LENGTH = 10 
  ISBN_MODULE = 11
  
  def valid?(isbn)
	hay_error = check_errors(isbn)
	if hay_error < 0
	  return false
	 end
    return ((calculate_check_digit(isbn) == isbn[-1]) ? true : false)
  end
  
  private
  
  def check_errors(isbn)
    return ((error_length = check_lenght(isbn)) < 0 ? error_length : check_digits(isbn))
  end
  
  def check_lenght(isbn)
    isbn = isbn.delete(' ').delete('-')
    if isbn.empty?
      return Errors::ERR_VACIO
    elsif isbn.length < ISBN_LENGTH
      return Errors::ERR_CORTO
    elsif isbn.length > ISBN_LENGTH
      return Errors::ERR_LARGO
    end
    return 0
  end
  
  def check_digits(isbn)
    isbn = isbn.delete(' ').delete('-')
    if isbn.scan(/\D/).empty?
      return 0
    end
    return Errors::ERR_NO_ES_NUMERICO
  end
  
  def calculate_check_digit(isbn)
    isbn = isbn.delete(' ').delete('-')
    check_digit_isbn = 0
    #Realizo un chop para no tomar el ultimo digito que es el que quiero calcular
    descomposicion_isbn = isbn.chop.chars.map(&:to_i)
    descomposicion_isbn.each_with_index do |num,index|
	  check_digit_isbn += num * (index + 1)
    end
    return ((digit = (check_digit_isbn % ISBN_MODULE)) == 10 ? 'X' : digit.to_s)
  end
end
