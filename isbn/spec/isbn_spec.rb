require 'rspec' 
require_relative '../model/isbn'

describe 'Isbn' do

  let(:isbn) { Isbn.new }  
   
  it 'valid? de vacio deberia ser false (es vacio)' do
    expect(isbn.valid?('')).to eq false
  end
  
  it 'valid? de 012345 deberia ser false (muy corto)' do
    expect(isbn.valid?('012345')).to eq false
  end
  
  it 'valid? de 012345678987 deberia ser false (muy largo)' do
    expect(isbn.valid?('012345678987')).to eq false
  end

  it 'valid? de 0123456789 deberia ser true' do
    expect(isbn.valid?('0123456789')).to eq true
  end

  it 'valid? de " 0 123 456 789" deberia ser true' do
    expect(isbn.valid?(' 0 123 456 789')).to eq true
  end

  it 'valid? de "0-123-45678-9" deberia ser true' do
    expect(isbn.valid?('0-123-45678-9')).to eq true
  end

  it 'valid? de a0123456789 deberia ser false (digitos invalidos)' do
    expect(isbn.valid?("a012345678")).to eq false
  end
  
  it 'valid? de 0-471-95869-7 deberia ser true' do
    expect(isbn.valid?("0-471-95869-7")).to eq true
  end
  
  it 'valid? de 0-471-95869-8 deberia ser false (digito de chequeo invalido)' do
    expect(isbn.valid?("0-471-95869-8")).to eq false
  end
  
end
