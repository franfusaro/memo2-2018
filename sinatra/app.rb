require 'sinatra'
require 'json'
require_relative 'model/chopper'

get '/' do
  'hello'
end

get '/hola' do
  nombre = params["nombre"]
  content_type :json
  { :saludo => "hola #{nombre}"}.to_json
end

post '/chau' do
  nombre = params["nombre"]
  content_type :json
  { :saludo => "chau #{nombre}"}.to_json
end

get '/sum' do
  content_type :json
  nums_a_sumar = params["x"]
  if nums_a_sumar.nil?
    { :error => "parametro incorrecto" }.to_json
  else
    chopper = Chopper.new
    result = chopper.sum(nums_a_sumar.split(",").map(&:to_i))
    content_type :json
    { :sum => nums_a_sumar, :resultado => result }.to_json
  end
end

post '/chop' do
  content_type :json
  num_a_buscar = params["x"]
  nums_donde_buscar = params["y"]
  if nums_donde_buscar.nil? || num_a_buscar.nil?
    { :error => "parametro incorrecto" }.to_json
  else
    array_chop = nums_donde_buscar.split(",").map(&:to_i)
    chopper = Chopper.new
    result = chopper.chop(num_a_buscar.to_i, array_chop)
    { :chop => "x=" + num_a_buscar + ",y=" + params["y"], :resultado => result }.to_json
  end
end
