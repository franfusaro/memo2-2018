class Chopper
	
  def chop(n, array)
    if array.empty?
      return -1
    end
    cont = 0
    array.each do |num|
      if num == n
        return cont
      end
      cont+=1
    end
  end
  
  def sum(array)
    separator = ','
    numero_a_nombre = {0 => 'cero', 1 => 'uno', 2 => 'dos', 3 => 'tres', 4 => 'cuatro', 5 => 'cinco', 8 => 'ocho', 9 => 'nueve'}
    if array.empty?
      return 'vacio'
    end
    sum = array.inject(0){|sum,x| sum + x }
    if sum >= 100
      return 'demasiado grande'
    end
    result = ''
    descomposicion_numero = sum.to_s.chars.map(&:to_i)
    descomposicion_numero.each_with_index do |num,index|
      if index == descomposicion_numero.size - 1
        result = result + numero_a_nombre[num]
      else
        result = result + numero_a_nombre[num] + separator
      end		
    end
    return result
  end
end
