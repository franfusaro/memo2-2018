# spec/app_spec.rb

require File.expand_path '../spec_helper.rb', __FILE__

describe "Sinatra Application" do

  it "get / should return hello" do
    get '/'
    expect(last_response).to be_ok
    expect(last_response.body).to include 'hello'
  end

  it "hola debe devolver el nombre" do
    get '/hola?nombre=nico'
    expect(last_response).to be_ok
    expect(last_response.body).to include 'hola nico'
  end

  it "chau debe devolver el nombre" do
    post '/chau', { 'nombre':'nico'}
    expect(last_response).to be_ok
    expect(last_response.body).to include 'chau nico'
  end
  
  it "get /sum?x= debe devolver vacio" do
    get '/sum?x='
    expect(last_response).to be_ok
    expect(last_response.body).to include '"resultado":"vacio"'
  end
  
  it "get /sum?x=4 debe devolver cuatro" do
    get '/sum?x=4'
    expect(last_response).to be_ok
    expect(last_response.body).to include '"resultado":"cuatro"'
    expect(last_response.body).to include '"sum":"4"'
  end
  
  it "get /sum?x=4,8,9 debe devolver cuatro" do
    get '/sum?x=4,8,9'
    expect(last_response).to be_ok
    expect(last_response.body).to include '"resultado":"dos,uno"'
    expect(last_response.body).to include '"sum":"4,8,9"'
  end
  
  it "get /sum?x=50,50 debe devolver demasiado grande" do
    get '/sum?x=50,50'
    expect(last_response).to be_ok
    expect(last_response.body).to include '"resultado":"demasiado grande"'
    expect(last_response.body).to include '"sum":"50,50"'
  end
  
  it "get /sum sin x debe ser parametro incorrecto" do
    get '/sum'
    expect(last_response).to be_ok
    expect(last_response.body).to include '"error":"parametro incorrecto"'
  end

  it "post /chop de x=3 e y vacio debe ser -1" do
    post '/chop', { 'x':'3', 'y':'' }
    expect(last_response).to be_ok
    expect(last_response.body).to include '"resultado":-1'
  end

  it "post /chop de x=3 e y 1,3,5 debe ser 1" do
    post '/chop', { 'x':'3', 'y':'1,3,5' }
    expect(last_response).to be_ok
    expect(last_response.body).to include '"chop":"x=3,y=1,3,5"'
    expect(last_response.body).to include '"resultado":1'
  end

  it "post /chop de x=3 e y vacio debe ser -1" do
    post '/chop', { 'x':'3', 'y':'' }
    expect(last_response).to be_ok
    expect(last_response.body).to include '"resultado":-1'
  end
  
  it "post /chop sin y debe ser parametro incorrecto" do
    post '/chop', { 'x':'3'}
    expect(last_response).to be_ok
    expect(last_response.body).to include '"error":"parametro incorrecto"'
  end

  it "post /chop sin x debe ser parametro incorrecto" do
    post '/chop', { 'y':'3'}
    expect(last_response).to be_ok
    expect(last_response.body).to include '"error":"parametro incorrecto"'
  end

end

# curl localhost:4567/hola?nombre=nico
# curl -X POST localhost:4567/chau -d "nombre=nico"



