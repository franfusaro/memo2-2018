Feature: Approval regime
  As a Student
  I want to know the approval regime related to a course 

  Background:
    Given the Student Juan
    Given the individual task ta
    Given the individual task tb
    Given the individual task tc
    Given the group project pg

  
  Scenario: Student approved
    Given the regular Student Juan
    When He approved all weekly tasks
    And He attended 75% of the classes
    And He approved at least 3 iterations of the proyect pg
    Then He approved the course
  
  Scenario: Student disapproved by attendance
    Given the regular Student Juan
    When He approved all weekly tasks
    And He attended 60% of the classes
    And He approved at least 3 iterations of the proyect pg
    Then He disapproved the course
  
  Scenario: Student disapproved by individual tasks
    Given the regular Student Juan
    When He does not approve the individual task tc
    Then he disapproved the course

  
  Scenario: Student disapproved by group project
    Given the regular Student Juan
    When He approved all weekly tasks
    And He attended 75% of the classes
    And He approved 2 iterations of the proyect pg
    Then He disapproved the course

 
