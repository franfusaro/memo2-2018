
Given(/^the Student Juan$/) do
  @student = Student.new
  @student.name = 'Juan'
end

Given(/^the individual task ta$/) do
  @ta = IndividualTask.new("ta")
end

Given(/^the individual task tb$/) do
  @tb = IndividualTask.new("tb")
end

Given(/^the individual task tc$/) do
  @tc = IndividualTask.new("tc")
end

Given(/^the group project pg$/) do
  @pg = GroupProject.new 
end

Given(/^the regular Student Juan$/) do
  @student.regular = true 
end

When(/^He approved all weekly tasks$/) do
  @student.approve_task(@ta)
  @student.approve_task(@tb)
  @student.approve_task(@tc)
end

When(/^He attended (\d+)% of the classes$/) do |classes_attended_percent|
  @student.attended_classes_percent = classes_attended_percent
end

When(/^He approved at least (\d+) iterations of the proyect pg$/) do |num_iterations|
  @pg.min_iterations_to_approve = num_iterations
  num_iterations.to_i.times do
    @student.approve_iteration(@pg)
  end
end

Then(/^He approved the course$/) do
  @course = Course.new
  @course.add_task(@ta)
  @course.add_task(@tb)
  @course.add_task(@tc)
  @course.add_group_project(@pg)
  @course.min_attendance_percent = 75
  @course.add_student(@student)
  expect(@course.approved_student?(@student)).to eq true
end

When(/^He does not approve the individual task tc$/) do
  @student.approve_task(@ta)
  @student.approve_task(@tb) 
end

Then(/^he disapproved the course$/) do
  @course = Course.new
  @course.add_task(@ta)
  @course.add_task(@tb)
  @course.add_task(@tc)
  @course.add_group_project(@pg)
  @course.min_attendance_percent = 75
  @course.add_student(@student)
  expect(@course.approved_student?(@student)).to eq false
end

When(/^He approved (\d+) iterations of the proyect pg$/) do |num_iterations|
  @pg.min_iterations_to_approve = 3
  num_iterations.to_i.times do
    @student.approve_iteration(@pg)
  end
end

Then(/^He disapproved the course$/) do
  @course = Course.new
  @course.add_task(@ta)
  @course.add_task(@tb)
  @course.add_task(@tc)
  @course.add_group_project(@pg)
  @course.min_attendance_percent = 75
  @course.add_student(@student)
  expect(@course.approved_student?(@student)).to eq false
end

