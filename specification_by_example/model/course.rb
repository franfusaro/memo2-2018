class Course
  attr_accessor :min_attendance_percent
  
  def initialize()
    @tasks = []
    @students = []
    @approved_students = []
  end
  
  def add_task(task)
    if @tasks.any? { |existing_task| existing_task.id == task.id }
      return -1
    end
    @tasks.push(task)
    return 0
  end
  
  def add_group_project(pg)
    @group_project = pg
    return 0
  end
  
  def add_student(student)
    @students.push(student)
    return 0
  end
  
  def get_students()
    return @students
  end
  
  def approved_student?(student)
    approved = check_student_approvation(student)
    if approved < 0
      return false
    end
    @approved_students.push(student)
    return true
  end
  
  private
  
  def check_student_approvation(student)
    if !@students.include?(student)
      return -1
    end
    if !approved_all_tasks?(student)
      return -2
    end
    if !has_min_attendance?(student)
      return -3
    end
    if !approved_min_iterations?(student)
      return -4
    end
    return 0
  end
  
  def approved_all_tasks?(student)
    @tasks.each do |task| 
      if !student.approved_task?(task)
        return false
      end
    end
    return true
  end
  
  def has_min_attendance?(student)
    if student.attended_classes_percent.to_i < @min_attendance_percent
      return false
    end
    return true
  end
  
  def approved_min_iterations?(student)
    if student.approved_iterations(@group_project).to_i >= @group_project.min_iterations_to_approve.to_i
      return true
    end
    return false
  end
end
