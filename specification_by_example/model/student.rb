class Student
  attr_accessor :name
  attr_accessor :regular
  attr_accessor :attended_classes_percent
  attr_accessor :approved_tasks
  
  def initialize()
    @approved_tasks = []
    @approved_iterations = Hash.new(0)
    @attended_classes_percent = 0
  end
  
  def approved_tasks()
    return @approved_tasks.count
  end
  
  def approve_task(task)
    if @approved_tasks.any? { |approved_task| approved_task.id == task.id }
      return -1
    end
    @approved_tasks.push(task)
    return 0
  end
  
  def approved_iterations(project)
    return @approved_iterations[project]
  end

  def approve_iteration(project)
    @approved_iterations[project] = @approved_iterations[project] + 1;
    return 0
  end
  
  def approved_task?(task)
     if @approved_tasks.any? { |approved_task| approved_task.id == task.id }
      return true
    end
    return false
  end
  
end
