require 'rspec' 
require_relative '../model/student'
require_relative '../model/individual_task'
require_relative '../model/group_project'
require_relative '../model/course'

describe 'Student' do
  describe 'model' do
    subject(@student) { Student.new }
    it { should respond_to(:name) }
    it { should respond_to(:regular) }
    it { should respond_to(:attended_classes_percent) }
  end
  
  let(:student) { Student.new }
  
  it 'approved tasks returns 0 if i dont add any tasks' do
    expect(student.approved_tasks()).to eq 0
  end
  
  it 'approve task with id ta returns 0 (ok)' do
    ta = IndividualTask.new("ta")
    expect(student.approve_task(ta)).to eq 0
  end
  
  it 'approved tasks after approving task with id ta returns 1' do
    ta = IndividualTask.new("ta")
    student.approve_task(ta)
    expect(student.approved_tasks()).to eq 1
  end
  
  it 'approved tasks after approving tasks with ids ta and tb returns 2' do
    ta = IndividualTask.new("ta")
    tb = IndividualTask.new("tb")
    student.approve_task(ta)
    student.approve_task(tb)
    expect(student.approved_tasks()).to eq 2
  end
  
  it 'approve task returns -1 if i try to approve twice a task with the same id' do
    ta = IndividualTask.new("ta")
    ta_aux = IndividualTask.new("ta")
    student.approve_task(ta)
    expect(student.approve_task(ta_aux)).to eq -1
  end
  
  it 'approved_task? returns false if i ask for a task that the student didnt approve' do
    ta = IndividualTask.new("ta")
    expect(student.approved_task?(ta)).to eq false
  end
  
  it 'approved_task? returns true if i ask for a task that the student already approved' do
    ta = IndividualTask.new("ta")
    student.approve_task(ta)
    expect(student.approved_task?(ta)).to eq true
  end
  
  it 'approved iterations returns 0 if the student didnt approve any iteration of pg' do
    pg = GroupProject.new
    expect(student.approved_iterations(pg)).to eq 0
  end
  
  it 'approve iteration for pg returns 0 (ok)' do
    pg = GroupProject.new
    expect(student.approve_iteration(pg)).to eq 0
  end
  
  it 'approved iterations after approving one returns 1' do
    pg = GroupProject.new
    student.approve_iteration(pg)
    expect(student.approved_iterations(pg)).to eq 1
  end
  
  
end

describe 'IndividualTask' do
  describe 'model' do
    subject(@individual_task) { IndividualTask.new("ta") }
    it { should respond_to(:id) }
  end
end

describe 'GroupProject' do
  describe 'model' do
    subject(@group_project) { GroupProject.new }
    it { should respond_to(:min_iterations_to_approve) }
  end
end

describe 'Course' do
  describe 'model' do
    subject(@course) { Course.new }
    it { should respond_to(:min_attendance_percent) }
  end
  let(:course) { Course.new }
  
  it 'add task with id ta returns 0 (ok)' do
    ta = IndividualTask.new("ta")
    expect(course.add_task(ta)).to eq 0
  end
  
  it 'add_task twice with the same id ta returns -1 (cant add the same task twice)' do
    ta = IndividualTask.new("ta")
    ta_aux = IndividualTask.new("ta")
    course.add_task(ta)
    expect(course.add_task(ta_aux)).to eq -1
  end
  
  it 'add group project to a course returns 0 (ok)' do
    pg = GroupProject.new
    expect(course.add_group_project(pg)).to eq 0
  end
  
  it 'add student to a course returns 0 (ok)' do
    student = Student.new
    expect(course.add_student(student)).to eq 0
  end
  
  it 'get students after adding two students returns a 2 elements array that contains them' do
    student = Student.new
    student_2 = Student.new
    course.add_student(student)
    course.add_student(student_2)
    expect(course.get_students()).to eq [student, student_2]
  end
  
  it 'approved_student? for a student that is not in the course returns false (student not enrolled in course)' do
    student = Student.new
    course = Course.new
    expect(course.approved_student?(student)).to eq false
  end
  
  it 'approved_student? for a student that is in the course but didnt approve all individual tasks returns false' do
    student = Student.new
    course = Course.new
    course.add_task(IndividualTask.new("ta"))
    course.add_student(student)
    expect(course.approved_student?(student)).to eq false
  end
  
  it 'approved_student? for a student that is in the course and approved all individual tasks but didnt reached the min attendance percent returns false' do
    student = Student.new
    course = Course.new
    ta = IndividualTask.new("ta")
    tb = IndividualTask.new("tb")
    course.add_task(ta)
    course.add_task(tb)
    student.approve_task(ta)
    student.approve_task(tb)
    course.add_student(student)
    course.min_attendance_percent = 75
    expect(course.approved_student?(student)).to eq false
  end
  
  it 'approved_student? for a student that is in the course, approved all individual tasks and the min attendance percent but didnt approve the min iterations of the group project returns false' do
    student = Student.new
    course = Course.new
    ta = IndividualTask.new("ta")
    tb = IndividualTask.new("tb")
    course.add_task(ta)
    course.add_task(tb)
    student.approve_task(ta)
    student.approve_task(tb)
    student.attended_classes_percent = 75
    pg = GroupProject.new
    course.add_group_project(pg)
    pg.min_iterations_to_approve = 3
    student.approve_iteration(pg)
    course.add_student(student)
    course.min_attendance_percent = 75
    expect(course.approved_student?(student)).to eq false
  end
  
  it 'approved_student? for a student that is in the course, approved all individual tasks, the min attendance percent and approved the min iterations of the group project returns true' do
    student = Student.new
    course = Course.new
    ta = IndividualTask.new("ta")
    tb = IndividualTask.new("tb")
    course.add_task(ta)
    course.add_task(tb)
    student.approve_task(ta)
    student.approve_task(tb)
    student.attended_classes_percent = 75
    pg = GroupProject.new
    course.add_group_project(pg)
    pg.min_iterations_to_approve = 3
    pg.min_iterations_to_approve.times do
      student.approve_iteration(pg)
    end
    course.add_student(student)
    course.min_attendance_percent = 75
    expect(course.approved_student?(student)).to eq true
  end
  
end
